package main

import (
	"fmt"
	"time"
)

func main() {
	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Xayrli tong!")
	case t.Hour() < 17:
		fmt.Println("Hayrli kun.")
	default:
		fmt.Println("Hayrli kech.")
	}
}
